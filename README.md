# 00 - README


**Developing a Computational Pharmacokinetic Model of Systemic Snakebite Envenomation and Antivenom Treatment**

The aim of this work was to build and parameterise a pharmacokinetic model of systemic snakebite envenomation and antivenom treatment. The models follow a classical two-compartment structure, whereby venom and antivenom are tracked across a central compartment of blood and well-perfused tissue, and a peripheral compartment of less well perfused tissue. Venom is intramuscularly absorbed into the central compartment, and antivenom is administered intravenously at a set rate. Antivenom can bind venom in either compartment, and neutralised venom-antivenom complexes adopt the same characteristics as the antivenom. All species are eliminated from the central compartment. All parameters used in this code were based on experimental values for venoms and antivenoms in rabbits. 

Users are advised to refer to the source paper for further information on the model design and parameterisation. 


## Folder contents

**1. 01_Parameter_Regressions.ipynb** <br>
    Contains the k<sub>10</sub>, k<sub>12</sub>, and k<sub>21</sub> rate constant regressions and scaffold parameter predictions (Fig. 3)
    
**2. 02_AV_Simulations.ipynb** <br>
    Contains the compartmental simulations of different antivenom scaffolds (Fig. 4)
    
**3. 03_Envenomation_Treatment_Simulation.ipynb** <br>
    Contains the compartmental simulations of envenomation and antivenom treatment (Fig. 5)
    
**4. 04_Variable_Envenomation.ipynb** <br>
    Contains scripts for simulation of variable venom dosing (Fig. 6) and variable venom absorption and bioavailabilities (Figs. S6-S8).
    
**5. 05_ESM_Simulations.ipynb** <br>
    Contains all additional simulations referenced in the supplementary materials (Figs. S1-S5)

**6. 06_Basic_Venom_AV_Solvers.ipynb** <br>
    Contains basic scripts for users to simulate the dynamics of venom, antivenom, and envenomation-treatment scenarios.   
    
    
## Setup

The project is written in .ipynb scripts. To run these, first-time users are encouraged to install Anaconda and access the scripts via the inbuilt IDEs JupyterLab/Jupyter Notebook. 

The scripts were written in JupyterLab version 1.2.6, with packages NumPy 1.18.1, matplotlib 3.1.3, and Scipy 1.4.1.

    
## References

1. Krifi MN, Miled K, Abderrazek M, El Ayeb M (2001) Effects of antivenom on Buthus occitanus tunetanus (Bot) scorpion venom pharmacokinetics: towards an optimization of antivenom immunotherapy in a rabbit model. Toxicon 39:1317–1326. https://doi.org/10.1016/S0041-0101(01)00083-6

2. Li Z, Krippendorff B-F, Shah DK (2017) Influence of molecular size on the clearance of antibody fragments. Pharm Res 34:2131–2141. https://doi.org/10.1007/s11095-017-2219-y

3. Shah DK, Betts AM (2013) Antibody biodistribution coefficients. mAbs 5:297–305. https://doi.org/10.4161/mabs.23684

4. Sim SM, Saremi K, Tan NH, Fung SY (2013) Pharmacokinetics of Cryptelytrops purpureomaculatus (mangrove pit viper) venom following intravenous and intramuscular injections in rabbits. International Immunopharmacology 17:997–1001. https://doi.org/10.1016/j.intimp.2013.10.007

5. Yap MKK, Tan NH, Sim SM, et al (2014) Pharmacokinetics of Naja sumatrana (Equatorial Spitting Cobra) Venom and Its Major Toxins in Experimentally Envenomed Rabbits. PLoS Negl Trop Dis 8:. https://doi.org/10.1371/journal.pntd.0002890

6. Yap MKK, Tan NH, Sim SM, Fung SY (2013) Toxicokinetics of Naja sputatrix (Javan spitting cobra) venom following intramuscular and intravenous administrations of the venom into rabbits. Toxicon 68:18–23. https://doi.org/10.1016/j.toxicon.2013.02.017

